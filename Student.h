/**
@file Student.h
This is the header file; declaration of class Student.

@author Hassan Kumail Ali (hassankumail9@gmail.com)
 
@version 1.0
*/



#include <iostream>
#include <string>
using namespace std;

/**
* @class Student
* This class declares the Student. 
* It contains all the data to be read from Sample-Gradebook.csv file like CMSID , firstname, lastname etc
* Contains two constructors.
* method calculateAggregate to calculate aggregates of all the students.
*/


class student {
public:

	/// Field variables that will store all the students data being read from Sample-Gradebook.csv.
	/// See Main.cpp for better understanding.
	string CMSID, firstname, lastname;
	double quiz1, ass1, ass2, ass3, quiz2, quiz3, oht1, oht2, ESE, aggregate;

	student();
	student(string cmsid, string fn, string ln, double q1, double a1, double a2, double a3, double q2, double q3, double o1, double o2, double ese);
	void calculateAggregate();

};
