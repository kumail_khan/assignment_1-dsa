/**
@file Student.cpp
This is the cpp file; implementation of class Student.

@author Hassan Kumail Ali (hassankumail9@gmail.com)

@version 1.0
*/


#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include "student.h"
#include <assert.h>
using namespace std;




/** Implementing the no-arg constructor of Student class */
student::student() {}


/**
* Implementation of Constructor to make objects of data read from Sample-Gradebook.csv file
* @param[in]   takes in all the details of the student and initialize the field variables.
*/


student::student(string cmsid, string fn, string ln, double q1, double a1, double a2, double a3, double q2, double q3, double o1, double o2, double ese) {
	CMSID = cmsid;
	firstname = fn;
	lastname = ln;

	quiz1 = q1;
	quiz2 = q2;
	quiz3 = q3;
	ass1 = a1;
	ass2 = a2;
	ass3 = a3;
	oht1 = o1;
	oht2 = o2;
	ESE = ese;
}
/**
* This function calculates aggregates of the student object.
* @param[in]   None
* @return      void
*/

void student::calculateAggregate() {
	aggregate = ((quiz1 + quiz2 + quiz3)*(10.0 / 30.0) + (ass1 + ass2 + ass3)*(10.0 / 30.0) + (oht1)*(20.0 / 50.0) + (oht2)*(20.0 / 50.0) + ESE * (40.0 / 100.0));

}